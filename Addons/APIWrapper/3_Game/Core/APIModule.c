class APIModule
{
	protected typename 	m_WMAttachType;
	protected int 		m_WMAttachIndex = -1;
	
	bool Attach(typename wrapperConnect)
	{
		int index = APICoreManager.GetInstance().Attach(wrapperConnect, this);
		if(index >= 0)
		{
			m_WMAttachType 	= wrapperConnect;
			m_WMAttachIndex = index;
			return true;
		} else Logger.Error(this, "Attach", "Module can't attached");
		return false;
	}
	
	bool Detach()
	{
		if(m_WMAttachIndex >= 0)
		{
			if(m_WMAttachType)
			{
				if(!APICoreManager.GetInstance().Detach(m_WMAttachType, m_WMAttachIndex)) Logger.Error(this, "Detach", "Error Detach Module");
				return true;
			} else Logger.Error(this, "Detach", "No exist type module");
		} else Logger.Error(this, "Detach", "Module don't attached");
		return false;
	}
	
	void OnCreate() {}
	void OnDestroy() {}
}