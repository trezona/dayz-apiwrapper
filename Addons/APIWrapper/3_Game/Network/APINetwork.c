class APINetwork : APIModule
{
	autoptr map<int, autoptr APINetworkConnection> 					connections		= new map<int, autoptr APINetworkConnection>();
	protected autoptr map<int, autoptr APINetworkMessageDelegate> 	handlers 		= new map<int, autoptr APINetworkMessageDelegate>();
	protected autoptr APINetworkConnection 							localConnection	= NULL;
	
	protected string 	addressListener;
	protected int 		maxConnections;

	void APINetwork(string addressListener)
	{
		this.addressListener = addressListener.Hash().ToString();
		//this.localConnection = new NetworkConnection(0);
	}
	
	bool dontListen = false;
	void OnRPConnections(int msgType, APINetworkMessage msg)
	{
		msg = handlers[msgType].ReadRPC(msg);
		if(msg.connectionId >= 0)
		{
			APINetworkConnection conn;
			if(connections.Find(msg.connectionId, conn))
			{
				if(!conn)
				{
					Logger.Error(this, "OnRPC", "null connectionId:" + msg.connectionId);
					connections.Remove(msg.connectionId);
					return;
				}
				
				connections[msg.connectionId].OnDataReceived(msg);
				return;
			}
			
			Logger.Error(this, "OnRPC", "Unknown connection: " + msg.connectionId);
			return;
		}
			
		for(int i = 0; i < connections.Count(); i++)
		{
			conn = connections.GetElement(i);
			if(!conn)
			{
				Logger.Error(this, "OnRPC", "null connection: " + msg.connectionId);
				connections.Remove(msg.connectionId);
				continue;
			}	
				
			conn.OnDataReceived(msg);
		}
	}
	
	void OnRPC(ref PlayerIdentity sender, ref Object target, int rpcType, ref ParamsReadContext ctx )
	{
		if(dontListen) return;
		
		// Check Packed ID && MSGType
		if(rpcType.ToString().Substring(0, addressListener.Length()) == addressListener) // Remove last number, from msgType
		{
			int msgType = rpcType.ToString().Substring(addressListener.Length(), rpcType.ToString().Length()).ToInt(); // Get Last Number, get MsgType

			if(!handlers.Contains(msgType)) // Check MsgType
			{
				Logger.Error(this, "OnRPC", "Unknown msgType:" + msgType);
				return;
			}
			
			OnRPConnections(msgType, new APINetworkMessage(sender, target, rpcType, ctx));
			return;
		}
		
		Logger.Error(this, "OnRPC", "Unknown rpcType:" + rpcType);
		Logger.Error(this, "OnRPC", "Unknown msgType CALC:" + rpcType.ToString().Substring(0, addressListener.Length()));
	}
	
	void SetMaxConnections(int maxConns)
	{
		autoptr APINetworkConnection networkConnection;
		
		while(connections.Count() > maxConns)
		{
			networkConnection = connections.GetElement(connections.Count() - 1);
			if(networkConnection)
			{
				//networkConnection.OnDestroy();
				delete networkConnection;
				connections.RemoveElement(connections.Count() - 1);
			}
		}
		maxConnections = maxConns;
	}

	bool AddConnection(APINetworkConnection conn)
	{		
		if (connections.Count() >= maxConnections)
		{
			Logger.Error(this, "AddConnection", "Maximum number of connections reached.");
			return false;
		}
		
		if (conn.GetConnectionID() < 0) 
		{
			Logger.Error(this, "AddConnection", "Invalid connectionId: " + conn.GetConnectionID() + " .");
			return false;
		}
		
		if (conn.GetConnectionID() == 0) 
		{
			Logger.Error(this, "AddConnection", "Invalid connectionId: " + conn.GetConnectionID() + " . Needs to be > 0, because 0 is reserved connection.");
			return false;
		}
	
		if (connections.Contains(conn.GetConnectionID()))
		{
			Logger.Error(this, "AddConnection", "Already a connection with this id");
			return false;
		}
		
		//conn.handlers = handlers;
		connections.Set(conn.GetConnectionID(), conn);
		return true;
	}
	
	bool RemoveConnection(int connectionId)
	{
		autoptr APINetworkConnection conn;
		if(connections.Find(connectionId, conn))
		{
			if(conn)
			{
				//conn.OnDestroy();
				delete conn;
			}
			
			connections.Remove(connectionId);
			return true;
		}
		return false;
	}
	
	bool SetLocalConnection(APINetworkConnection conn)
	{
		if(conn)
		{
			if(localConnection)
			{
				Logger.Error(this, "SetLocalConnection", "Connection already exists");
				return false;
			}
			
			if(conn.GetConnectionID() != 0)
			{
				Logger.Error(this, "SetLocalConnection", "Invalid connectionId: " + conn.GetConnectionID() + ". Needed 0 for local connection.");
				return false;
			}
			
			localConnection = conn;
			return true;
		}
		return false;
	}
	
	bool RemoveLocalConnection()
	{
		if(!localConnection)
		{
			Logger.Error(this, "RemoveLocalConnection", "Connection not exists");
			return false;
		}
		
		//localConnection.OnDestroy();
		delete localConnection;
		localConnection = NULL;
		
		return true;
	}
	
	void DisconnectAll()
	{
		autoptr APINetworkConnection conn;
		for(int i = 0; i < connections.Count(); i++)
		{
			conn = connections.GetElement(i);
			if(conn)
			{
				//conn.OnDestroy();
				delete conn;
			}
		}
		connections.Clear();
	}
	
	void RemoveHandler(int msgType)
	{
		int serverType = (addressListener + msgType).ToInt();
		autoptr APINetworkMessageDelegate msg;
		if(handlers.Contains(serverType))
		{
			
			msg = handlers[serverType];
			if(msg) delete msg;
			handlers.Remove(serverType);
			return;
		}
		
		Logger.Error(this, "RemoveHandler", "Handler msgType not exist");
	}
	
	void RegisterHandler(int msgType, ref APINetworkMessageDelegate handler)
	{
		if(!handler)
		{
			Logger.Error(this, "RegisterHandler", "Handler is NULL");
			return;
		}
		
		int serverType = (addressListener + msgType).ToInt();
		if (handlers.Contains(serverType))
		{
			Logger.Full(this, "RegisterHandler", "msgType Replacing: " + msgType);
		}
		handlers.Set(serverType, handler);
	}
	
	// ===========================
	// ======= SEND METHOD =======
	//bool SendCommandToAll(int msgType, NetworkCommandMessage msg) {}
	//bool SendCommandToClient(int msgType, NetworkCommandMessage msg) {}
	//bool SendCommandToConnection(int msgType, NetworkCommandMessage msg) {}
	
	void SendToAll(int msgType, APINetworkMessageBase msg)
	{
		for(int i = 0; i < connections.Count(); i++)
		{
			msg.msgType		 	= msgType;
			msg.networkAddress	= addressListener;
			
			connections.GetElement(i).Send(msg);
		}
		if(connections.Count() == 0) Logger.Error(this, "SendToAll", "Failed to send message, not found connection.");
	}
	
	void SendToConnection(int connectionId, int msgType, APINetworkMessageBase msg)
	{
		autoptr APINetworkConnection conn;
		if(connections.Contains(connectionId))
		{
			if(!conn)
			{
				Logger.Error(this, "SendToConnection", "Failed to send message to connection ID '" + connectionId + "', null connection.");
				connections.Remove(connectionId);
				return;
			}
			
			msg.connectionId 	= connectionId;
			msg.msgType		 	= msgType;
			msg.networkAddress	= addressListener;
			
			connections.Get(connectionId).Send(msg);
			return;
		}
		Logger.Error(this, "SendToConnection", "Failed to send message to connection ID '" + connectionId + "', not found in connection list.");
	}
	
	void SendToClient(APINetworkIdentity networkIdentity, int msgType, APINetworkMessageBase msg)
	{
		if(networkIdentity)
		{
			msg.networkIdentity = networkIdentity;
			msg.msgType 		= msgType;
			msg.networkAddress	= addressListener;
			
			autoptr APINetworkConnection conn;
			for(int i = 0; i < connections.Count(); i++)
			{
				conn = connections.GetElement(i);
				if(!conn) 
				{
					connections.RemoveElement(i);
					continue;
				}
				
				conn.Send(msg);
			}
			return;
		}
		Logger.Error(this, "SendToClientOfPlayer", "Player has no NetworkIdentity: " + networkIdentity.GetPlayerName());
	}
	
	//bool ReplacePlayerForConnection(NetworkConnection conn, NetworkIdentity player) {}
	//bool AddPlayerForConnection(NetworkConnection conn, NetworkIdentity player) {}
	//bool SetupLocalPlayerForConnection(NetworkConnection conn, NetworkIdentity identity) {}
	//bool InternalReplacePlayerForConnection(NetworkConnection conn, NetworkIdentity player) {}
	//bool GetNetworkIdentity(Object go, out NetworkIdentity identity) {}
}