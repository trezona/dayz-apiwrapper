class APINetworkHandlerPacket : APINetworkMessageDelegate
{
	APINetworkMessage ReadRPC(APINetworkMessage msg)
	{
		if(msg.GetContext().Read(msg)) return msg;
		return NULL;
	}
}