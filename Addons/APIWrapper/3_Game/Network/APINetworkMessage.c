class APINetworkMessage : APINetworkMessageBase
{
	protected autoptr Param1<ref ParamsReadContext> context;
	protected autoptr Param1<ref PlayerIdentity> 	sender;
	protected autoptr Param1<ref Object> 			target;
	protected int 							rpcType;
	
	ref ParamsReadContext 	GetContext() 	return context.param1;
	ref PlayerIdentity 		GetSender() 	return sender.param1;
	ref Object 				GetTarget() 	return target.param1;
	int 						GetID() 	return rpcType;
	
	void APINetworkMessage(ref PlayerIdentity sender, ref Object target, int rpcType, ref ParamsReadContext context)
	{
		this.sender		= new Param1<ref PlayerIdentity>(sender);
		this.target		= new Param1<ref Object>(target);
		this.rpcType	= rpcType;
		this.context 	= new Param1<ref ParamsReadContext>(context);
	}
}