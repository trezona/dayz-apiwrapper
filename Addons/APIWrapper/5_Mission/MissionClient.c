modded class MissionGameplay
{
	void MissionGameplay()
	{
		APICoreManager.CreateInstance();
		DayZGame().Event_OnRPC.Insert(OnRPC);
		APICoreManager.SendCommand("GameStart");
	}
	
	void ~MissionGameplay()
	{
		APICoreManager.SendCommand("GameStop");
		DayZGame().Event_OnRPC.Remove(OnRPC);
		APICoreManager.DeleteInstance();
	}
	
	void OnRPC( PlayerIdentity sender, Object target, int rpc_type, ParamsReadContext ctx )
	{
		APICoreManager.SendCommand("OnRPC", new Param4<ref PlayerIdentity, ref Object, int, ref ParamsReadContext>(sender, target, rpc_type, ctx));
	}
	
	override void OnInit()
	{	
		super.OnInit();
		APICoreManager.SendCommand("OnInit");
	}
	
	bool HasMissionLoaded = false;
	void OnMissionLoaded()
	{
		HasMissionLoaded = true;
		APICoreManager.SendCommand("OnMissionLoaded");
	}
	
	override void OnUpdate(float timeslice)
	{
		super.OnUpdate(timeslice);
		
		if(!HasMissionLoaded && !GetDayZGame().IsLoading()) OnMissionLoaded();
		APICoreManager.SendCommand("OnUpdate", new Param1<float>(timeslice));
	}
	
	override void OnEvent(EventType eventTypeId, Param params)
	{
		super.OnEvent(eventTypeId, params);
		APICoreManager.SendCommand("OnEvent", new Param2<EventType, ref Param>(eventTypeId, params));
	}
	
	
	// OTHER
	override void InitInventory()
	{
		super.InitInventory();
		APICoreManager.SendCommand("InitInventory");
	}

	override void OnMissionStart()
	{
		super.OnMissionStart();
		APICoreManager.SendCommand("OnMissionStart");
	}
	
	override void OnMissionFinish()
	{
		super.OnMissionFinish();
		APICoreManager.SendCommand("OnMissionFinish");
	}
	
	override void AddDummyPlayerToScheduler(Man player)
	{
		super.AddDummyPlayerToScheduler(player);
		APICoreManager.SendCommand("AddDummyPlayerToScheduler", new Param1<ref Man>(player));
	}
	override void Reset()
	{
		super.Reset();
		APICoreManager.SendCommand("Reset");
	}
	override void ResetGUI()
	{
		super.ResetGUI();
		APICoreManager.SendCommand("ResetGUI");
	}
	override void CreateLogoutMenu(UIMenuPanel parent)
	{
		super.CreateLogoutMenu(parent);
		APICoreManager.SendCommand("CreateLogoutMenu", new Param1<ref UIMenuPanel>(parent));
	}
	override void OnKeyPress(int key)
	{
		super.OnKeyPress(key);
		APICoreManager.SendCommand("OnKeyPress", new Param1<int>(key));
	}
	override void OnKeyRelease(int key)
	{
		super.OnKeyRelease(key);
		APICoreManager.SendCommand("OnKeyRelease", new Param1<int>(key));
	}
	override void OnMouseButtonPress(int button)
	{
		super.OnKeyRelease(button);
		APICoreManager.SendCommand("OnMouseButtonPress", new Param1<int>(button));
	}
	override void OnMouseButtonRelease(int button)
	{
		super.OnMouseButtonRelease(button);
		APICoreManager.SendCommand("OnMouseButtonRelease", new Param1<int>(button));
	}	
	override void OnItemUsed(InventoryItem item, Man owner)
	{
		super.OnItemUsed(item, owner);
		APICoreManager.SendCommand("OnItemUsed", new Param2<ref InventoryItem, ref Man>(item, owner));
	}
	override void PlayerControlDisable(int mode)
	{
		super.PlayerControlDisable(mode);
		APICoreManager.SendCommand("PlayerControlDisable", new Param1<int>(mode));
	}
	override void StartLogoutMenu(int time)
	{
		super.StartLogoutMenu(time);
		APICoreManager.SendCommand("StartLogoutMenu", new Param1<int>(time));
	}
	override void ShowChat()
	{
		super.ShowChat();
		APICoreManager.SendCommand("ShowChat");
	}
	override void HideChat()
	{
		super.HideChat();
		APICoreManager.SendCommand("HideChat");
	}
	override void ShowVehicleInfo()
	{
		super.ShowVehicleInfo();
		APICoreManager.SendCommand("ShowVehicleInfo");
	}
	override void HideVehicleInfo()
	{
		super.HideVehicleInfo();
		APICoreManager.SendCommand("HideVehicleInfo");
	}
	override void Pause()
	{
		super.Pause();
		APICoreManager.SendCommand("Pause");
	}
	override void Continue()
	{
		super.Continue();
		APICoreManager.SendCommand("Continue");
	}
	override void AbortMission()
	{
		super.AbortMission();
		APICoreManager.SendCommand("AbortMission");
	}
	override void CreateDebugMonitor()
	{
		super.CreateDebugMonitor();
		APICoreManager.SendCommand("CreateDebugMonitor");
	}
	override void UpdateDebugMonitor()
	{
		super.UpdateDebugMonitor();
		APICoreManager.SendCommand("UpdateDebugMonitor");
	}
	override void RefreshCrosshairVisibility()
	{
		super.RefreshCrosshairVisibility();
		APICoreManager.SendCommand("RefreshCrosshairVisibility");
	}
	override void HideCrosshairVisibility()
	{
		super.HideCrosshairVisibility();
		APICoreManager.SendCommand("HideCrosshairVisibility");
	}
	override void PlayerControlEnable()
	{
		super.PlayerControlEnable();
		APICoreManager.SendCommand("PlayerControlEnable");
	}
	override void ShowInventory()
	{
		super.ShowInventory();
		APICoreManager.SendCommand("ShowInventory");
	}
	override void HideInventory()
	{
		super.HideInventory();
		APICoreManager.SendCommand("HideInventory");
	}
}