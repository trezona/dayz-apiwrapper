modded class MissionServer
{
	void MissionServer()
	{
		APICoreManager.CreateInstance();
		DayZGame().Event_OnRPC.Insert(OnRPC);
		APICoreManager.SendCommand("GameStart");
	}
	
	void ~MissionServer()
	{
		APICoreManager.SendCommand("GameStop");
		DayZGame().Event_OnRPC.Remove(OnRPC);
		APICoreManager.DeleteInstance();
	}
	
	void OnRPC( PlayerIdentity sender, Object target, int rpc_type, ParamsReadContext ctx )
	{
		APICoreManager.SendCommand("OnRPC", new Param4<ref PlayerIdentity, ref Object, int, ref ParamsReadContext>(sender, target, rpc_type, ctx));
	}
	
	override void OnInit()
	{	
		super.OnInit();
		APICoreManager.SendCommand("OnInit");
	}
	
	bool HasMissionLoaded = false;
	void OnMissionLoaded()
	{
		HasMissionLoaded = true;
		APICoreManager.SendCommand("OnMissionLoaded");
	}
	
	override void OnUpdate(float timeslice)
	{
		super.OnUpdate( timeslice );
		
		if(!HasMissionLoaded && !GetDayZGame().IsLoading()) OnMissionLoaded();
		APICoreManager.SendCommand("OnUpdate", new Param1<float>(timeslice));
	}
	
	override void OnEvent(EventType eventTypeId, Param params)
	{
		super.OnEvent(eventTypeId, params);
		APICoreManager.SendCommand("OnEvent", new Param2<EventType, ref Param>(eventTypeId, params));
	}
	
	
	// OTHER
	override void InvokeOnConnect( PlayerBase player, PlayerIdentity identity)
	{
		super.InvokeOnConnect( player, identity );
		APICoreManager.SendCommand("InvokeOnConnect", new Param2<ref PlayerBase, ref PlayerIdentity>(player, identity));
	} 

	override void InvokeOnDisconnect( PlayerBase player )
	{
		super.InvokeOnDisconnect( player );
		APICoreManager.SendCommand("InvokeOnDisconnect", new Param1<ref PlayerBase>(player));
	}
	
	override void EquipCharacter()
	{
		super.EquipCharacter( );
		APICoreManager.SendCommand("EquipCharacter");
	}
	
	override void StartingEquipSetup(PlayerBase player, bool clothesChosen)
	{
		super.StartingEquipSetup(player, clothesChosen);
		APICoreManager.SendCommand("StartingEquipSetup", new Param2<ref PlayerBase, bool>(player, clothesChosen));
	}
	
	override void OnClientReadyEvent(PlayerIdentity identity, PlayerBase player)
	{
		super.OnClientReadyEvent(identity, player);
		APICoreManager.SendCommand("OnClientReadyEvent", new Param2<ref PlayerIdentity, ref PlayerBase>(identity, player));
	}
	
	override void OnClientRespawnEvent(PlayerIdentity identity, PlayerBase player)
	{
		super.OnClientRespawnEvent(identity, player);
		APICoreManager.SendCommand("OnClientRespawnEvent", new Param2<ref PlayerIdentity, ref PlayerBase>(identity, player));
	}
	
	override void OnClientReconnectEvent(PlayerIdentity identity, PlayerBase player)
	{
		super.OnClientReconnectEvent(identity, player);
		APICoreManager.SendCommand("OnClientReconnectEvent", new Param2<ref PlayerIdentity, ref PlayerBase>(identity, player));
	}
	
	override void OnClientDisconnectedEvent(PlayerIdentity identity, PlayerBase player, int logoutTime, bool authFailed)
	{
		super.OnClientDisconnectedEvent(identity, player, logoutTime, authFailed);
		APICoreManager.SendCommand("OnClientDisconnectedEvent", new Param4<ref PlayerIdentity, ref PlayerBase, int, bool>(identity, player, logoutTime, authFailed));
	}
	
	override void PlayerDisconnected(PlayerBase player, PlayerIdentity identity, string uid)
	{
		super.PlayerDisconnected(player, identity, uid);
		APICoreManager.SendCommand("PlayerDisconnected", new Param3<ref PlayerBase, ref PlayerIdentity, string>(player, identity, uid));
	}
	
	override void HandleBody(PlayerBase player)
	{
		super.HandleBody(player);
		APICoreManager.SendCommand("HandleBody", new Param1<ref PlayerBase>(player));
	}
}
