﻿class CfgPatches	// требуется обязательно
{
	class APIWrapper	// требуется обязательно, совпадает с именем мода
	{
		requiredVersion=1; 		// may be not worked
		requiredAddons[]={};
		/*
			requiredAddons[]=
			{
				"DZ_Data",		// Перечисляем требуемые и используемые модом аддоны (pbo-файлы игры и модов по их внутренним скриптовым именам)
			};
		*/
	};
};
class CfgMods // // требуется обязательно, совпадает с именем мода
{
	// https://community.bistudio.com/wiki/DayZ:Modding_Structure#Mod_presentation
	class APIWrapper
	{
	    dir = "APIWrapper";							// Имя папки мода, совпадает с именем мода
	    picture = "APIWrapper/data/api_infographic_smartfile_crop.paa"; 									// Картинка в описании мода
	    action = "http://vk.com/trezona";  									// Ссылка, например вот так: action = "https://dayz.com/";	
	    hideName = 1;									// Не уверен, что этот параметр работает
	    hidePicture = 1;								// Не уверен, что этот параметр работает
	    name = "APIWrapper";  						// Имя мода (а также его внутреннее скриптовое имя)
		logoSmall = "APIWrapper/data/api_infographic_smartfile_crop.paa";	// значок рядом с именем мода, если описание не развернуто
		logo = "APIWrapper/data/api_infographic_smartfile_crop.paa";				// логотип под меню игры
		logoOver = "Mods/TestMod/modlogohover.tga";		// при наведении курсора мыши на логотип
		tooltip = "API Framework";							// подсказка при наведении курсора мыши
		overview = "Library for mods"; 					// Описание
	    credits = "vk.com/trezona";								// credits
	    author = "Trezona";								// author
	    authorID = "trezona";  								// author steam ID 
	    version = "2.0";  								// version
	    extra = 0;										// Не уверен, что этот параметр работает
		
	    type = "mod"; 									// требуется обязательно, остается неизменным
		// inputs = "mods\testmod\inputs\my_new_inputs.xml"; 	     // необязательно, при использовании пользовательских inputs
		dependencies[]={ "Core", "Game", "World", "Mission" };  							// необязательно, если необходимо установить зависимость класса

	    class defs
	    {			
			class engineScriptModule
			{
				value=""; // если значение пустое, используется функция ввода по умолчанию
				files[]={"APIWrapper/1_Core"}; // вы можете добавить любое количество файлов или каталогов, и они будут скомпилированы вместе с оригинальными скриптами игрового модуля
			};
			class gameLibScriptModule
			{
				value="";
				files[]={"APIWrapper/2_GameLib"};
			};
			class gameScriptModule
			{
				//value="CreateGameMod"; // когда значение заполнено, имя функции ввода модуля скрипта по умолчанию перезаписывается им
				value = "";
				files[]={"APIWrapper/3_Game"};
			};
			class worldScriptModule
			{
				value="";
				files[]={"APIWrapper/4_World"};
			};
			class missionScriptModule
			{
				value="";
				files[]={"APIWrapper/5_Mission"};
			};
        };
		// Если какой-то из разделов скриптинга вам не нужен и вы не сибираетесь его паковать, сотрите необходимый блок с ним выше и в массиве dependencies 
    };
};